package com.example.aplicaciondegps;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.tasks.OnSuccessListener;

public class Inicio extends AppCompatActivity {
    private FusedLocationProviderClient Ubicacion;
    FirebaseDatabase database;
    DatabaseReference ubicacion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);

        database=FirebaseDatabase.getInstance();
        ubicacion=database.getReference("Mi ubicacion");
        Button btninicio=findViewById(R.id.btnenviar);

        btninicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compartirbicacion();
            }
        });
    }
    private void conpartirUbicacion(){

        if(ContextCompat.checkSelfPermission(Inicio.this, Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED){
        }
        else{
            ActivityCompat.requestPermissions(Inicio.this, new String[]{
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION},1);
        }
        Ubicacion= LocationServices.getFusedLocationProviderClient(Inicio.this);
        Ubicacion.getLastLocation().addOnSuccessListener(Inicio.this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if(location != null){
                    EditText usuarionombre=findViewById(R.id.edtusuario);
                    String usuario=usuarionombre.getText().toString();
                    Double latitud=location.getLatitude();
                    Double longitud=location.getLongitude();
                    Toast.makeText(Inicio.this, "Latitud: " + latitud + "Longitud: " + longitud, Toast.LENGTH_LONG).show();
                    ubicacion obj= new  ubicacion(usuario,latitud,longitud);
                    ubicacion.push().setValue(obj);
                    Intent irMapa = new Intent(Inicio.this,MapsActivity.class);
                    startActivity(irMapa);

                }
            }
        });

    }
    public void irMapa(View view)
    {
        Intent Inicio=new Intent(Inicio.this,MapsActivity.class);
        startActivity(Inicio);
    }

}

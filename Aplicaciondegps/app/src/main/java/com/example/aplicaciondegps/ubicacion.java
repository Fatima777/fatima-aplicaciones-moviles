package com.example.aplicaciondegps;

public class ubicacion {
    String Nombre;
    Double Latitud;
    Double Longitud;

    public ubicacion() {
    }

    public ubicacion(String nombre, Double latitud, Double longitud) {
        Nombre = nombre;
        Latitud = latitud;
        Longitud = longitud;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public Double getLatitud() {
        return Latitud;
    }

    public void setLatitud(Double latitud) {
        Latitud = latitud;
    }

    public Double getLongitud() {
        return Longitud;
    }

    public void setLongitud(Double longitud) {
        Longitud = longitud;
    }
}

}

package com.example.noticiasupt;
import androidx.appcompat.app.AppCompatActivity;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Registro extends AppCompatActivity {
    private Button btnregistro;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        btnregistro = findViewById(R.id.btnregistro);
        final EditText edtusua = findViewById(R.id.edtusua);
        final EditText edcontra = findViewById(R.id.edtContra);
        final EditText edtconfi = findViewById(R.id.edtconfi);

        Toast.makeText(Registro.this,"Te conectaste", Toast.LENGTH_LONG).show();

        /*yacuenta=(TextView) findViewById(R.id.textViewcuenta);
        yacuenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( Registro.this,MainActivity.class);

                startActivity(intent);
            }
        });*/
        btnregistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText nombreusuario=(EditText)findViewById(R.id.edtusua);
                final EditText contraseña1=(EditText)findViewById(R.id.edtContra);
                EditText contraseña2=(EditText)findViewById(R.id.edtconfi);

                if (nombreusuario.getText().toString()==""){
                    nombreusuario.setSelectAllOnFocus(true);
                    nombreusuario.requestFocus();
                    return;
                }

                if(contraseña1.getText().toString()==""){
                    contraseña1.setSelectAllOnFocus(true);
                    contraseña1.requestFocus();
                    return;
                }
                if(contraseña2.getText().toString()==""){
                    contraseña2.setSelectAllOnFocus(true);
                    contraseña2.requestFocus();
                    return;
                }
                if(!contraseña1.getText().toString().equals(contraseña2.getText().toString())){
                    Toast.makeText(Registro.this,"Las contraseñas no coiciden", Toast.LENGTH_LONG).show();

                }


                ServicioPeticion service= Api.getApi(Registro.this).create(ServicioPeticion.class);
                Call<Registro_Usuario> registroCall=service.registrarUsuario(nombreusuario.getText().toString(),contraseña1.getText().toString());
                registroCall.enqueue(new Callback<Registro_Usuario>(){
                    @Override
                    public void onResponse(Call<Registro_Usuario>call, Response<Registro_Usuario> response){
                        Registro_Usuario peticion =response.body();
                        if(response.body()==null){
                            Toast.makeText(Registro.this,"Ocurrio un Error; Intentelo mas tarde", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if(peticion.estado=="true"){
                            startActivity(new Intent(Registro.this,MainActivity.class));
                            Toast.makeText(Registro.this,"Datos registrados",Toast.LENGTH_SHORT).show();

                        }else{
                            Toast.makeText(Registro.this,peticion.detalle,Toast.LENGTH_LONG).show();
                        }

                    }
                    @Override
                    public void onFailure(Call<Registro_Usuario>call,Throwable t){
                        Toast.makeText(Registro.this,"Error",Toast.LENGTH_LONG).show();
                    }

                });


            }


        });

    }

}
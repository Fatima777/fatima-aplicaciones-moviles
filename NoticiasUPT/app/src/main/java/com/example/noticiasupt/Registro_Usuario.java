package com.example.noticiasupt;

public class Registro_Usuario {

    public String estado;
    public String correo;
    public String pasword;
    public String detalle;


    public  Registro_Usuario(String correo, String pasword ){
        this.correo=correo;
        this.pasword=pasword;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getPasword() {
        return pasword;
    }

    public void setPasword(String pasword) {
        this.pasword = pasword;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }
}

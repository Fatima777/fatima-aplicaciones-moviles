package com.example.noticiasupt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Button btninicio;
    TextView cuenta;
    private final String mensaje="Estas en registro";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final EditText edtcorreo= findViewById(R.id.edtCorreo);
        final EditText edtcontraseña= findViewById(R.id.edtcontraseña);
        cuenta=(TextView) findViewById(R.id.textViewcuenta);
        cuenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( MainActivity.this,Registro.class);
                intent.putExtra("texto",mensaje);
                startActivity(intent);
            }
        });
    }
}
